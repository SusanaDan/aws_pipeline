# VPC
resource "aws_vpc" "vpc-sv-tf" {
  cidr_block = "10.0.0.0/16"
tags = {
  Name = "vpc-sv-tf2"
}
}
#SUBNET
resource "aws_subnet" "subnet-sv-tf" {
  vpc_id     = aws_vpc.vpc-sv-tf.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "subnet-sv2"
  }
}

#IGW
resource "aws_internet_gateway" "igw-sv-tf" {
  vpc_id = aws_vpc.vpc-sv-tf.id

  tags = {
    Name = "igw-sv2"
  }
}

#Routr table
resource "aws_route_table" "rt-sv-tf" {
  vpc_id = aws_vpc.vpc-sv-tf.id


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-sv-tf.id
  }

  tags = {
    Name = "rt-sv2"
  }
}

#route table association
resource "aws_route_table_association" "rta-sv" {
  subnet_id      = aws_subnet.subnet-sv-tf.id
  route_table_id = aws_route_table.rt-sv-tf.id
}


#security group
resource "aws_security_group" "sgSUVETERRA" {
name = "sgSUVETERRA"
vpc_id = aws_vpc.vpc-sv-tf.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress{
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
     ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
   Name = "sgSUVE2"
}
}

resource "aws_key_pair" "kp-sv-tf" {
  key_name   = "sv-key2"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC7ZzQ5BdqHBA2jhTqBuc0rGmI5U5vI8AfND/z+pblVYASLJ/ltRvV+8IkMKJf+FtSJm/TE3OIJn0yUn7ngQCwWIkcyoveyCprONp1lvz8TgJH2VUDEiU5hnO1tXtFvGbgDKhS6W25GexrQbSSPmTVJ/JsJk3MyGtBtMws6l2FntK3ieLuy/TnLm5SHPWxTojpN335ITiOdgBWXFAy8MQRQZTznAHGfL+gt0k8RapBvfXhS28qfgVD6dyEZtTcjZbopWG9h4WzdpRTyvThEmn/HZ4NVy6lB7G5GKFoCcPqFChPPqNTcMj2zAjmsDubCwiRXMTu4rkwVCNlRZ1sMgQ8uUpKuHrwpoEUF+TdowVbKU9glzcYDgT5gnyFnE6GupeXHJoAGh9uK+A3Q1oqg5XU56YEew/0PFapiqUQr600EGH2rGs/ZRfsh5wBReS6BjzF6FjiBosd5vT9sqSIAtpqIONjhFI3a1QWZXlo17q3TQ0VNcVjkmL3aS3iUDH7bygE= susanavega@SusanaVega-MacBookPro.local"
}



resource "aws_instance" "ins-sv-tf" {
  ami = "ami-0d5eff06f840b45e9"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sgSUVETERRA.id]
  subnet_id = aws_subnet.subnet-sv-tf.id 
}


#resource "aws_route" "r" {
#  route_table_id = aws_route_table.rt-sv-tf.id
#}
